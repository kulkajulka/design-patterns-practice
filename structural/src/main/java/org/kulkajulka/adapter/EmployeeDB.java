package org.kulkajulka.adapter;

public class EmployeeDB implements Employee {
	private String firstName;
	private String lastName;
	private String id;
	private String emailAddress;

	public EmployeeDB(String firstName, String lastName, String id, String emailAddress) {
		this.firstName = firstName;
		this.lastName = lastName;
		this.id = id;
		this.emailAddress = emailAddress;
	}

	@Override
	public String getId() {
		return id;
	}

	@Override
	public String getFirstName() {
		return firstName;
	}

	@Override
	public String getLastName() {
		return lastName;
	}

	@Override
	public String getEmailAddress() {
		return emailAddress;
	}
}
