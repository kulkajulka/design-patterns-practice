package org.kulkajulka.adapter;

import java.util.ArrayList;
import java.util.List;

public class EmployeeClient {

	public List<Employee> getEmployeeList() {
		List<Employee> employees = new ArrayList<>();
		Employee employee = new EmployeeDB("Julia", "Rossi", "id0", "julia.rossi@mail.com");
		employees.add(employee);

		//cannot be done
		// Employee ldapEmployee = new EmployeeLdap("Matt", "Wick", "567","matt@wick.com");

		EmployeeLdap employeeLdap = new EmployeeLdap("13", "Wick", "Matt", "matt@wick.com");

		employees.add(new EmployeeAdapterLdap(employeeLdap));

		EmployeeCsv employeeCsv = new EmployeeCsv("123,julia,bianchi,julia@bianchi.com");

		employees.add(new EmployeeAdapterCsv(employeeCsv));

		return employees;
	}
}
