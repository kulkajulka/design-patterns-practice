package org.kulkajulka.adapter;

import java.util.StringTokenizer;

public class EmployeeCsv {

	private int id;
	private String firstname;
	private String lastname;
	private String emailAddress;

	public EmployeeCsv(String values) {
		StringTokenizer tokenizer = new StringTokenizer(values, ",");
		if (tokenizer.hasMoreElements()) {
			this.id = Integer.parseInt(tokenizer.nextToken());
		}
		if (tokenizer.hasMoreElements()) {
			this.firstname = tokenizer.nextToken();
		}
		if (tokenizer.hasMoreElements()) {
			this.lastname = tokenizer.nextToken();
		}
		if (tokenizer.hasMoreElements()) {
			this.emailAddress = tokenizer.nextToken();
		}
	}

	public int getId() {
		return id;
	}

	public String getFirstname() {
		return firstname;
	}

	public String getLastname() {
		return lastname;
	}

	public String getEmailAddress() {
		return emailAddress;
	}
}
