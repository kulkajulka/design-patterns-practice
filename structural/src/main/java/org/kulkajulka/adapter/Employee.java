package org.kulkajulka.adapter;

public interface Employee {
	String getId();
	String getFirstName();
	String getLastName();
	String getEmailAddress();
}
