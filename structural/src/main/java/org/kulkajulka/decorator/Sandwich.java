package org.kulkajulka.decorator;

public interface Sandwich {
	String make();
}
