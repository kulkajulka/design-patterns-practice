package org.kulkajulka.composite;

public class Menu extends MenuComponent {
	public Menu(String name, String url) {
		this.name = name;
		this.url = url;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append(print(this));
		menuComponents.forEach(each -> builder.append(each.toString()));
		return builder.toString();
	}

	@Override
	public MenuComponent add(MenuComponent menuComponent) {
		menuComponents.add(menuComponent);
		return menuComponent;
	}

	@Override
	public MenuComponent remove(MenuComponent menuComponent) {
		menuComponents.remove(menuComponent);
		return menuComponent;
	}


}
