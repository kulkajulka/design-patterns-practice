package org.kulkajulka.singleton;

public class MySingleton {
	// making it thread-safe
	private static volatile MySingleton instance = null;

	// prevent from being instantiated with reflection
	private MySingleton() {
		if (instance != null) {
			throw new RuntimeException("Use getInstance() method to create");
		}
	}

	public static MySingleton getInstance() {
		// lazy-loading
		if (instance == null) {
			// again prevent to threads to create this instance in the same time
			synchronized (MySingleton.class){
				if (instance == null) {
					instance = new MySingleton();
				}
			}
		}
		return instance;
	}

}
