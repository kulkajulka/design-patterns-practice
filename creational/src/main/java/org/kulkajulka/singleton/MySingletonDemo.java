package org.kulkajulka.singleton;

public class MySingletonDemo {
	public static void main(String[] args) {
		MySingleton instance = MySingleton.getInstance();
		System.out.println(instance);
		MySingleton instance2 = MySingleton.getInstance();
		System.out.println(instance2);

		if (instance == instance2) {
			System.out.println("They are the same instance");
		}
	}
}
