package org.kulkajulka.singleton;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

public class DBSingletonDemo {
	public static void main(String[] args) {
		long timeBefore = 0;
		long timeAfter = 0;
		DBSingleton dbSingleton = DBSingleton.getInstance();

		timeBefore = System.currentTimeMillis();
		Connection connection = dbSingleton.getConnection();
		timeAfter = System.currentTimeMillis();

		System.out.println(timeAfter - timeBefore);

		Statement statement;

		try {
			statement = connection.createStatement();
			int count = statement.executeUpdate(
					"CREATE TABLE address (id INT NOT NULL GENERATED ALWAYS AS IDENTITY, street_name VARCHAR(255), city VARCHAR(255))");
			System.out.println("Table created.");
			statement.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}

		timeBefore = System.currentTimeMillis();
		connection = dbSingleton.getConnection();
		timeAfter = System.currentTimeMillis();

		System.out.println(timeAfter - timeBefore);
	}
}
