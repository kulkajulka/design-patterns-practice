package org.kulkajulka.singleton;

public class EverydayExample {
	public static void main(String[] args) {
		Runtime runtime = Runtime.getRuntime();
		System.out.println(runtime);
		Runtime runtime1 = Runtime.getRuntime();
		System.out.println(runtime);
		if (runtime == runtime1) {
			System.out.println("They are the same instance");
		}
	}
}
