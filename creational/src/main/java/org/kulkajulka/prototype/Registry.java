package org.kulkajulka.prototype;

import java.util.HashMap;
import java.util.Map;

public class Registry {
	private Map<String,  Item> items = new HashMap<String, Item>();

	public Registry() {
		loadItems();
	}

	public Item createItem(String type) {
		Item item = null;

		try {
			item = (Item)(items.get(type)).clone(); // casting, not nice
		} catch (CloneNotSupportedException e) {
			e.printStackTrace();
		}

		return item;
	}

	/* Just to laod initial data */
	private void loadItems() {
		Movie movie = new Movie();
		movie.setTitle("Basic movie");
		movie.setPrice(24.99);
		movie.setRuntime("2h");
		items.put("Movie",movie);

		Book book = new Book();
		book.setTitle("Basic Book");
		book.setPrice(12.99);
		book.setNumberOfPages(599);
		items.put("Book", book);
	}
}
