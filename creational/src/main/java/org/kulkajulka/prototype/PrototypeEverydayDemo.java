package org.kulkajulka.prototype;

import java.util.ArrayList;
import java.util.List;

public class PrototypeEverydayDemo {
	public static void main(String[] args) {
		String sql = "select * from movies where title = ?";
		List<String> parameters = new ArrayList<String>();

		parameters.add("Star wars");
		Record record = new Record();
		Statement statement = new Statement(sql, parameters, record);
		System.out.println(statement.getSql());
		System.out.println(statement.getParameters());
		System.out.println(statement.getRecord());

		Statement st = statement.clone();
		System.out.println(st.getSql());
		System.out.println(st.getParameters());
		System.out.println(st.getRecord()); // returns the same record (shallow clone)
	}
}
