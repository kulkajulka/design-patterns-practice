package org.kulkajulka.abstractfactory;

import static org.kulkajulka.abstractfactory.CardType.GOLD;

public class AbstractFactoryDemo {
	public static void main(String[] args) {
		CreditCardFactory abstractFactory = CreditCardFactory.getCreditCardFactory(700);
		CreditCard creditCard = abstractFactory.getCreditCard(CardType.PLATINUM);
		System.out.println(creditCard.getClass());
		abstractFactory = CreditCardFactory.getCreditCardFactory(600);
		CreditCard card = abstractFactory.getCreditCard(GOLD);
		System.out.println(card.getClass());
	}
}
