package org.kulkajulka.abstractfactory;

public enum CardType {
	GOLD, PLATINUM
}
