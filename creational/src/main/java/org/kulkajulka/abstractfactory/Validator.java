package org.kulkajulka.abstractfactory;

public interface Validator {
	boolean isValid(CreditCard creditCard);
}
