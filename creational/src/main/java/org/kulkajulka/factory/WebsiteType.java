package org.kulkajulka.factory;

public enum WebsiteType {
	BLOG, SHOP
}
