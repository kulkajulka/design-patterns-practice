package org.kulkajulka.builder;

/**
 * No setters provide immutability, org.kulkajulka.builder's methods give you opportunity to choose ingredients
 */
public class BirthdayCake {
	public static  class  Builder {
		private String taste;
		private Integer layers;
		private String fruits;
		private String text;

		public BirthdayCake build() {
			return new BirthdayCake(this);
		}

		public Builder taste(String taste) {
			this.taste = taste;
			return this;
		}

		public Builder layers(Integer layers) {
			this.layers = layers;
			return this;
		}

		public Builder fruits(String fruits) {
			this.fruits = fruits;
			return this;
		}

		public Builder text(String text) {
			this.text = text;
			return this;
		}
	}

	private String taste;
	private Integer layers;
	private String fruits;
	private String text;

	private BirthdayCake(Builder builder) {
		this.taste = builder.taste;
		this.layers = builder.layers;
		this.fruits = builder.fruits;
		this.text = builder.text;
	}

	public String getTaste() {
		return taste;
	}

	public Integer getLayers() {
		return layers;
	}

	public String getFruits() {
		return fruits;
	}

	public String getText() {
		return text;
	}
}
