package org.kulkajulka.builder;

/**
 * Telescoping constructors, preventing from creating exceptional cake without fruits, but with text
 */
public class BirthCakeTelescope {
	private String taste;
	private Integer layers;
	private String fruits;
	private String text;

	public BirthCakeTelescope(String taste) {
		this.taste = taste;
	}

	public BirthCakeTelescope(String taste, Integer layers) {
		this(taste);
		this.layers = layers;
	}

	public BirthCakeTelescope(String taste, Integer layers, String fruits) {
		this(taste, layers);
		this.fruits = fruits;
	}

	public BirthCakeTelescope(String taste, Integer layers, String fruits, String text) {
		this(taste, layers, fruits);
		this.text = text;
	}

	public String getTaste() {
		return taste;
	}

	public Integer getLayers() {
		return layers;
	}

	public String getFruits() {
		return fruits;
	}

	public String getText() {
		return text;
	}
}
