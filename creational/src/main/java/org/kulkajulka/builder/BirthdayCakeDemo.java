package org.kulkajulka.builder;

public class BirthdayCakeDemo {
	public static void main(String[] args) {
		BirthdayCake.Builder builder = new BirthdayCake.Builder();
		builder.taste("chocolate").layers(3).fruits("cherries").text("Happy Birthday");

		BirthdayCake birthdayCake = builder.build();

		System.out.println(birthdayCake.getTaste());
		System.out.println(birthdayCake.getLayers());
		System.out.println(birthdayCake.getFruits());
		System.out.println(birthdayCake.getText());
	}
}
