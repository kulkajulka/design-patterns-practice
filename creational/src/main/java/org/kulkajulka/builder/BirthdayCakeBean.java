package org.kulkajulka.builder;

/**
 * Getters and setters exposed, not immutable
 */
public class BirthdayCakeBean {
	private String taste;
	private Integer layers;
	private String fruits;
	private String text;

	public String getTaste() {
		return taste;
	}

	public void setTaste(String taste) {
		this.taste = taste;
	}

	public Integer getLayers() {
		return layers;
	}

	public void setLayers(Integer layers) {
		this.layers = layers;
	}

	public String getFruits() {
		return fruits;
	}

	public void setFruits(String fruits) {
		this.fruits = fruits;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}
}
